<?php

/*
**    Includes
*/
require __DIR__ . '/../../vendor/autoload.php';


/*
**    Init
*/
date_default_timezone_set('UTC');


/*
**  App
*/
$app = new \Slim\App();


/*
**  Routes
*/

$app->get('/', function($request, $response) {
    $response->write('Hello world!');
});
